"""Test cases for the servicers.
"""

import unittest

from protubufs import authorize_pb2, user_pb2
from service import servicers


class TestAuthenticateCredentialsServicer(unittest.TestCase):
    """Tests the `AuthenticateCredentialsServicer`.
    """

    def setUp(self):
        self.servicer = servicers.AuthenticationServicer()

    def test_invalid_token_format(self):
        mock_context = {}
        mock_request = authorize_pb2.Credentials(token='abcd')
        response = self.servicer.VerifyToken(mock_request, mock_context)

        # Checks if the response is empty.
        expected_response = user_pb2.User()
        self.assertEqual(response, expected_response)

    def test_invalid_token(self):
        mock_context = {}
        mock_request = authorize_pb2.Credentials(token='abcd.abcd.abc')
        response = self.servicer.VerifyToken(mock_request, mock_context)

        # Checks if the response is empty.
        expected_response = user_pb2.User()
        self.assertEqual(response, expected_response)

    def test_valid_token(self):
        mock_context = {}
        mock_request = authorize_pb2.Credentials(
            token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6ImFiYzEyMyIsIm5hbWUiOiJpa2l0aGluamkiLCJlbWFpbCI6Imlubm9jZW50QGlraXRoaW5qaS5jb20ifQ.H0h28CezyeqajN-DqRPR0cfaGgWZf4gN4wyC1ANXGOU')
        response = self.servicer.VerifyToken(mock_request, mock_context)

        expected_response = user_pb2.User(id='abc123', name='ikithinji',
                                          email='innocent@ikithinji.com')
        self.assertEqual(response, expected_response)
