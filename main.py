from service import logger, settings
from service.app import get_server


_logger = logger.setup_logging(__name__)

if __name__ == '__main__':
    _logger.info("Running")
    server = get_server()
    bind_address = f'[::]:{settings.SERVER_PORT}'
    server.add_insecure_port(bind_address)
    _logger.info('Server started on: %s', bind_address)
    server.start()
    server.wait_for_termination()
