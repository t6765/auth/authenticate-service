"""THe settings/configs file for the service.
"""
import os
from enum import Enum, auto


class Environment(Enum):
    """The environments where the service can be deployed on.
    """
    DEVELOPMENT = auto()
    PRODUCTION = auto()


ENVIRONMENT = Environment[os.environ.get('ENVIRONMENT', 'DEVELOPMENT')]

ENTERPRISE_PORTAL_SECRET = os.environ.get('ENTERPRISE_PORTAL_SECRET', '')

SERVER_PORT = os.environ.get('PORT', '5001')
