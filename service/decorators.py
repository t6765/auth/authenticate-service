"""Service decorators
"""

def log_rpc_call_start(rpc_name):
    """Adds the `request received` log at the start of the method call.

    Args:
        rpc_name: The str name of the rpc.
    """
    def inner(rpc):
        def wrapper(*args, **kwargs):
            request_payload = kwargs.get('request', 'None')
            args[0].logger.info(
                'Request received',
                extra={'rpc': rpc_name, 'request_payload': request_payload})
            return rpc(*args, **kwargs)
        return wrapper
    return inner

def log_rpc_call_end(rpc_name):
    """Adds the `request serviced` log at the end of the method call.

    Args:
        rpc_name: The str name of the rpc.
    """
    def inner(rpc):
        def wrapper(*args, **kwargs):
            response = rpc(*args, **kwargs)
            request_payload = kwargs.get('request', 'None')
            args[0].logger.info(
                'Request serviced',
                extra={'rpc': rpc_name, 'request_payload': request_payload})
            return response
        return wrapper
    return inner