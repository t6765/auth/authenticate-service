"""The custom log module for the service
"""

import logging

from pythonjsonlogger import jsonlogger

from service import settings


class StackdriverFormatter(jsonlogger.JsonFormatter, object):
    """Log formatter for Stackdriver logging
    """

    def __init__(
            self, *args, fmt='', style='%',
            **kwargs):  # pylint: disable=unused-argument
        jsonlogger.JsonFormatter.__init__(self, fmt=fmt, *args, **kwargs)

    def process_log_record(self, log_record):
        log_record['severity'] = log_record['levelname']
        del log_record['levelname']
        return super(StackdriverFormatter, self).process_log_record(
            log_record)


def setup_logging(name):
    """Sets up the logging for the module.

    The Python logging is setup with the StackdriverFormatter formatter. This
    outputs the logs as JSON.

    Typical usage example:
        LOGGER = setup_logging(__name__)
        LOGGER.info('Hello world')

        LOGGER.info('Adding numbers', extra={'a': 1, 'b': 2})

    Args:
        name: The string name of the module

    Returns:
        An instance of the Python logger.
    """
    print("setting up logger", settings.ENVIRONMENT, settings.Environment.PRODUCTION)
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)

    if settings.ENVIRONMENT is settings.Environment.PRODUCTION:
        # If the environment is production, set the Stackdriver specific
        # logging format.
        formatter = StackdriverFormatter(
            fmt='%(levelname) %(asctime) %(module) %(message)')
    else:
        # For local development, the built in Formatter class is used.
        formatter = logging.Formatter(
            fmt='%(levelname)s %(module)s | %(message)s')
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    if not logger.hasHandlers():
        logger.setLevel(logging.DEBUG)
        logger.addHandler(handler)

    return logger
