"""This application runs the GRPC service
"""

from concurrent import futures

import grpc

from protubufs import authorize_pb2_grpc
from service import servicers


def get_server() -> grpc.Server:
    """Creates the grpc server
    """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    servicer = servicers.AuthenticationServicer()
    authorize_pb2_grpc.add_AuthenticateServicer_to_server(servicer, server)

    return server
