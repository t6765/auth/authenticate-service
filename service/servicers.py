import jwt

from service.decorators import log_rpc_call_end, log_rpc_call_start
from protubufs import authorize_pb2_grpc, user_pb2
from service import logger, settings


class AuthenticationServicer(authorize_pb2_grpc.AuthenticateServicer):

    def __init__(self, *args, **kwargs) -> None:
        self.logger = logger.setup_logging(self.__class__.__name__)
        super(AuthenticationServicer).__init__(*args, **kwargs)

    def _build_response_body(self, token_payload=None):
        """Builds the response body.

       Args:
           token_payload: The payload in the authentication token.

       Returns:
           An instance of the protobufs.user_pb2.User with the user information.
       """
        user = user_pb2.User()
        if token_payload is None:
            return user
        else:
            user.id = token_payload.get('id', '')
            user.name = token_payload.get('name', '')
            user.email = token_payload.get('email', '')
            return user

    def _validate_token(self, token):
        """Checks if the token is valid.

        Args:
            token: The authorization token to validate.

        Raises:
            HTTPUnauthorized: The toek is invalid.
        """
        try:
            payload = jwt.decode(
                token, settings.ENTERPRISE_PORTAL_SECRET, algorithms=["HS256"])
        except (
                jwt.exceptions.DecodeError,
                jwt.exceptions.InvalidSignatureError) as e:
            self.logger.exception('Token is invalid')
            raise e
        self.logger.info(
            'Validated the token. Token is valid',
            extra={'token_payload': payload})
        return payload

    @log_rpc_call_start('VerifyToken')
    @log_rpc_call_end('VerifyToken')
    def VerifyToken(self, request, context):
        """RPC service for verifying jwt auth token

        Args:
            request: The protobuf message sent with the request

            context: Context for the request
        """
        try:
            token_payload = self._validate_token(request.token)
        except (
                jwt.exceptions.DecodeError,
                jwt.exceptions.InvalidSignatureError):

            response = self._build_response_body()
        else:
            response = self._build_response_body(token_payload=token_payload)

        return response
